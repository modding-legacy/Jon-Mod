package me.xjon.jons_mod.entity;

import net.minecraft.entity.EntityType;
import net.minecraft.entity.monster.SlimeEntity;
import net.minecraft.world.World;

/**
 * The Cheeseburger entity. In this case, it does nothing special, so it extends
 * the slime. This is the least amount of code you need to create a basic
 * entity.
 * 
 * @author Bailey
 */
public class CheeseburgerEntity extends SlimeEntity
{
	public CheeseburgerEntity(EntityType<? extends CheeseburgerEntity> type, World worldIn)
	{
		super(type, worldIn);
	}
}
