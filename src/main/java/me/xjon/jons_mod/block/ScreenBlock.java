
package me.xjon.jons_mod.block;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockReader;

public class ScreenBlock extends Block
{
	public ScreenBlock(Properties properties)
	{
		super(properties.setEmmisiveRendering(ScreenBlock::isEmissive));
	}

	@Override
	public float getAmbientOcclusionLightValue(BlockState state, IBlockReader worldIn, BlockPos pos)
	{
		return 1.0F;
	}

	@Override
	public boolean propagatesSkylightDown(BlockState state, IBlockReader reader, BlockPos pos)
	{
		return true;
	}

	private static boolean isEmissive(BlockState state, IBlockReader reader, BlockPos pos)
	{
		return true;
	}
}
