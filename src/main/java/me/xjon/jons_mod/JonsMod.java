package me.xjon.jons_mod;

import me.xjon.jons_mod.client.JonClientEvents;
import me.xjon.jons_mod.client.JonEntityRendering;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.DistExecutor;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;

/**
 * The main class.
 */
@Mod(JonsMod.MODID)
public class JonsMod
{
	public static final String NAME = "Jon's Mod";
	public static final String MODID = "jon";

	public static ResourceLocation locate(String name)
	{
		return new ResourceLocation(JonsMod.MODID, name);
	}

	public static String find(String name)
	{
		return JonsMod.MODID + ":" + name;
	}

	public JonsMod()
	{
		DistExecutor.unsafeRunWhenOn(Dist.CLIENT, () -> () -> FMLJavaModLoadingContext.get().getModEventBus().addListener(this::clientInit));

		MinecraftForge.EVENT_BUS.register(new JonEvents());
	}

	public void clientInit(FMLClientSetupEvent event)
	{
		MinecraftForge.EVENT_BUS.register(new JonClientEvents());

		JonEntityRendering.init();
	}
}
