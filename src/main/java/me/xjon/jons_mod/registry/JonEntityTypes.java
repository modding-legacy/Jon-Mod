package me.xjon.jons_mod.registry;

import java.util.Random;

import me.xjon.jons_mod.JonRegistry;
import me.xjon.jons_mod.JonsMod;
import me.xjon.jons_mod.client.JonEntityRendering;
import me.xjon.jons_mod.entity.CheeseburgerEntity;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityClassification;
import net.minecraft.entity.EntitySpawnPlacementRegistry;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.MobEntity;
import net.minecraft.entity.SpawnReason;
import net.minecraft.entity.ai.attributes.GlobalEntityTypeAttributes;
import net.minecraft.entity.monster.MonsterEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IWorld;
import net.minecraft.world.gen.Heightmap;
import net.minecraftforge.event.RegistryEvent.Register;

/**
 * The Entity Type registry. When adding a new entity, you add it here. When
 * doing so, remember to also register a render in {@link JonEntityRendering}
 */
public class JonEntityTypes
{
	public static final EntityType<CheeseburgerEntity> CHEESEBURGER = buildEntity("cheeseburger", EntityType.Builder.create(CheeseburgerEntity::new, EntityClassification.MONSTER).size(4.04F, 3.04F));

	public static void init(Register<EntityType<?>> event)
	{
		JonRegistry.register(event.getRegistry(), "cheeseburger", CHEESEBURGER);

		registerSpawnConditions();
		registerAttributes();
	}

	private static <T extends Entity> EntityType<T> buildEntity(String key, EntityType.Builder<T> builder)
	{
		return builder.build(JonsMod.find(key));
	}

	/**
	 * Attribute registry, needed for every entity. You can see a list of vanilla
	 * attributes in {@link GlobalEntityTypeAttributes}
	 */
	private static void registerAttributes()
	{
		GlobalEntityTypeAttributes.put(JonEntityTypes.CHEESEBURGER, MonsterEntity.func_234295_eP_().create());
	}

	/**
	 * Spawn conditions, used to check weather an entity can spawn or not. This is
	 * used by natural spawning, and spawners. This is NOT used by spawn eggs, or by
	 * things like arrows when shot.
	 */
	private static void registerSpawnConditions()
	{
		EntitySpawnPlacementRegistry.register(JonEntityTypes.CHEESEBURGER, EntitySpawnPlacementRegistry.PlacementType.ON_GROUND, Heightmap.Type.MOTION_BLOCKING_NO_LEAVES, SpawnConditions::mobSpawnConditions);
	}

	public static class SpawnConditions
	{
		public static boolean mobSpawnConditions(EntityType<? extends MobEntity> type, IWorld worldIn, SpawnReason reason, BlockPos pos, Random randomIn)
		{
			return MobEntity.canSpawnOn(type, worldIn, reason, pos, randomIn);
		}
	}
}
