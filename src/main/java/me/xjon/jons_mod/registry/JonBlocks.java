package me.xjon.jons_mod.registry;

import java.util.LinkedHashMap;
import java.util.Map;

import me.xjon.jons_mod.JonRegistry;
import me.xjon.jons_mod.block.ScreenBlock;
import net.minecraft.block.Block;
import net.minecraft.block.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraftforge.event.RegistryEvent.Register;
import net.minecraftforge.registries.IForgeRegistry;

/**
 * Block registry. The static initializers are usually kept together based on
 * how similar they are. (EX: screening blocks are kept together)
 */
public class JonBlocks
{
	public static Block jon_block, stella_block, red_b_block;

	public static Block green_screen_block, red_screen_block, blue_screen_block;

	public static Map<Block, ItemGroup> blockItemMap = new LinkedHashMap<>();
	public static Map<Block, Item.Properties> blockItemPropertiesMap = new LinkedHashMap<>();

	private static IForgeRegistry<Block> iBlockRegistry;

	// @formatter:off
	public static void init(Register<Block> event)
	{
		JonBlocks.iBlockRegistry = event.getRegistry();

		jon_block = register("jon_block", new Block(Block.Properties.from(Blocks.ANVIL)));
		stella_block = register("stella_block", new Block(Block.Properties.from(Blocks.ORANGE_WOOL)));
		red_b_block = register("red_b_block", new Block(Block.Properties.from(Blocks.RED_TERRACOTTA)));

		green_screen_block = register("green_screen_block", new ScreenBlock(Block.Properties.from(Blocks.LIME_WOOL).setLightLevel((value) -> 15)));
		red_screen_block = register("red_screen_block", new ScreenBlock(Block.Properties.from(Blocks.RED_WOOL).setLightLevel((value) -> 15)));
		blue_screen_block = register("blue_screen_block", new ScreenBlock(Block.Properties.from(Blocks.BLUE_WOOL).setLightLevel((value) -> 15)));
	}
	// @formatter:on

	public static void setBlockRegistry(IForgeRegistry<Block> iBlockRegistry)
	{
		JonBlocks.iBlockRegistry = iBlockRegistry;
	}

	public static Block register(String name, Block block)
	{
		register(name, block, ItemGroup.BUILDING_BLOCKS);
		return block;
	}

	public static <T extends ItemGroup> Block register(String key, Block block, T itemGroup)
	{
		blockItemMap.put(block, itemGroup);
		return registerBlock(key, block);
	}

	public static Block registerBlock(String name, Block block)
	{
		if (iBlockRegistry != null)
			JonRegistry.register(iBlockRegistry, name, block);

		return block;
	}
}