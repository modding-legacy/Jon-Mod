package me.xjon.jons_mod.registry;

import java.util.Map.Entry;

import me.xjon.jons_mod.JonRegistry;
import net.minecraft.block.Block;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.ArmorItem;
import net.minecraft.item.ArmorMaterial;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.SpawnEggItem;
import net.minecraftforge.event.RegistryEvent.Register;
import net.minecraftforge.registries.IForgeRegistry;

/**
 * Block registry. The static initializers are usually kept together based on
 * how similar they are. In this example, spawns eggs are kept together, while
 * the cheese item is seperate.
 */
public class JonItems
{
	public static Item cheese;

	public static Item render_helmet;

	public static Item cheeseburger_spawn_egg;

	private static IForgeRegistry<Item> iItemRegistry;

	public static void init(Register<Item> event)
	{
		JonItems.iItemRegistry = event.getRegistry();
		registerBlockItems();

		cheeseburger_spawn_egg = register("cheeseburger_spawn_egg", new SpawnEggItem(JonEntityTypes.CHEESEBURGER, 0x40362a, 0xeec634, new Item.Properties().group(ItemGroup.MISC)));

		cheese = register("cheese", new Item(new Item.Properties().group(ItemGroup.FOOD)));

		render_helmet = register("render_helmet", new ArmorItem(ArmorMaterial.IRON, EquipmentSlotType.HEAD, (new Item.Properties()).group(ItemGroup.COMBAT)));
	}

	private static void registerBlockItems()
	{
		for (Entry<Block, ItemGroup> entry : JonBlocks.blockItemMap.entrySet())
		{
			JonRegistry.register(iItemRegistry, entry.getKey().getRegistryName().getPath(), new BlockItem(entry.getKey(), new Item.Properties().group(entry.getValue())));
		}
		JonBlocks.blockItemMap.clear();

		for (Entry<Block, Item.Properties> entry : JonBlocks.blockItemPropertiesMap.entrySet())
		{
			JonRegistry.register(iItemRegistry, entry.getKey().getRegistryName().getPath(), new BlockItem(entry.getKey(), entry.getValue()));
		}

		JonBlocks.blockItemPropertiesMap.clear();
	}

	private static Item register(String name, Item item)
	{
		JonRegistry.register(iItemRegistry, name, item);
		return item;
	}
}