package me.xjon.jons_mod;

import me.xjon.jons_mod.registry.JonBlocks;
import me.xjon.jons_mod.registry.JonEntityTypes;
import me.xjon.jons_mod.registry.JonItems;
import net.minecraft.block.Block;
import net.minecraft.entity.EntityType;
import net.minecraft.item.Item;
import net.minecraftforge.event.RegistryEvent.Register;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;
import net.minecraftforge.registries.IForgeRegistry;
import net.minecraftforge.registries.IForgeRegistryEntry;

/**
 * Registry events. You shouldn't need to touch these unless you're adding a new
 * type of registry. (EX: biomes, since those aren't here.)
 * 
 * @author Bailey
 */
@EventBusSubscriber(modid = JonsMod.MODID, bus = Bus.MOD)
public class JonRegistry
{
	@SubscribeEvent
	public static void onRegisterItems(Register<Item> event)
	{
		JonItems.init(event);
	}

	@SubscribeEvent
	public static void onRegisterBlocks(Register<Block> event)
	{
		JonBlocks.init(event);
	}

	@SubscribeEvent
	public static void onRegisterEntityTypes(Register<EntityType<?>> event)
	{
		JonEntityTypes.init(event);
	}

	public static <T extends IForgeRegistryEntry<T>> void register(IForgeRegistry<T> registry, String name, T object)
	{
		object.setRegistryName(JonsMod.locate(name));
		registry.register(object);
	}
}