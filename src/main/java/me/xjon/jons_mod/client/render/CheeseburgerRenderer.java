package me.xjon.jons_mod.client.render;

import com.mojang.blaze3d.matrix.MatrixStack;

import me.xjon.jons_mod.JonsMod;
import me.xjon.jons_mod.client.model.CheeseburgerModel;
import me.xjon.jons_mod.entity.CheeseburgerEntity;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.entity.MobRenderer;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.MathHelper;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class CheeseburgerRenderer<T extends CheeseburgerEntity> extends MobRenderer<T, CheeseburgerModel<T>>
{
	private static final ResourceLocation TEXTURE = JonsMod.locate("textures/entity/cheeseburger.png");

	public CheeseburgerRenderer(EntityRendererManager manager)
	{
		super(manager, new CheeseburgerModel<>(), 0.25F);
	}

	@Override
	public void render(T entityIn, float entityYaw, float partialTicks, MatrixStack matrixStackIn, IRenderTypeBuffer bufferIn, int packedLightIn)
	{
		this.shadowSize = 0.25F * (float) entityIn.getSlimeSize();
		super.render(entityIn, entityYaw, partialTicks, matrixStackIn, bufferIn, packedLightIn);
	}

	@Override
	protected void preRenderCallback(T entitylivingbaseIn, MatrixStack matrixStackIn, float partialTickTime)
	{
		matrixStackIn.scale(0.999F, 0.999F, 0.999F);
		matrixStackIn.translate(0.0D, (double) 0.001F, 0.0D);
		float f1 = (float) entitylivingbaseIn.getSlimeSize();
		float f2 = MathHelper.lerp(partialTickTime, entitylivingbaseIn.prevSquishFactor, entitylivingbaseIn.squishFactor) / (f1 * 0.5F + 1.0F);
		float f3 = 1.0F / (f2 + 1.0F);
		matrixStackIn.scale(f3 * f1, 1.0F / f3 * f1, f3 * f1);
	}

	@Override
	public ResourceLocation getEntityTexture(T entity)
	{
		return TEXTURE;
	}
}