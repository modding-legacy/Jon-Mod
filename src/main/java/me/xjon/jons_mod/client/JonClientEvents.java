package me.xjon.jons_mod.client;

import me.xjon.jons_mod.registry.JonItems;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import net.minecraftforge.client.event.EntityViewRenderEvent;
import net.minecraftforge.event.TickEvent.ClientTickEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;

/**
 * Client specific events.
 */
public class JonClientEvents
{
	@SubscribeEvent
	public void onClientTick(ClientTickEvent event)
	{
		Minecraft mc = Minecraft.getInstance();
		World world = mc.world;
		PlayerEntity player = mc.player;

		if (world == null || !world.isRemote)
		{
			return;
		}

		ItemStack helmet = player.getItemStackFromSlot(EquipmentSlotType.HEAD);

		// Render Helmet
		if (helmet.getItem() == JonItems.render_helmet)
		{
			if (!mc.isGamePaused())
			{
				// Project.gluPerspective(fovy, aspect, zNear, zFar);
				mc.gameSettings.fov = 2.0D;
				player.rotationYaw = 135.0F;
				player.rotationPitch = 30.0F;
			}
		}
		else if (helmet.getItem() != JonItems.render_helmet && mc.gameSettings.fov == 2.0D)
		{
			mc.gameSettings.fov = 80.0D;
		}
	}

	@SubscribeEvent
	public void cameraSetup(EntityViewRenderEvent.CameraSetup event)
	{
		Minecraft mc = Minecraft.getInstance();
		PlayerEntity player = mc.player;

		ItemStack helmet = player.getItemStackFromSlot(EquipmentSlotType.HEAD);

		if (helmet.getItem() == JonItems.render_helmet)
		{
			if (!mc.isGamePaused())
			{
				// Project.gluPerspective(fovy, aspect, zNear, zFar);
				mc.gameSettings.fov = 2.0D;
				event.setYaw(135.0F);
				event.setPitch(30.0F);
			}
		}
		else if (helmet.getItem() != JonItems.render_helmet && mc.gameSettings.fov == 2.0D)
		{
			mc.gameSettings.fov = 80.0D;
		}
	}
}
