package me.xjon.jons_mod.client.model;

import com.google.common.collect.ImmutableList;

import net.minecraft.client.renderer.entity.model.SegmentedModel;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.entity.LivingEntity;
import net.minecraft.util.math.MathHelper;

public class CheeseburgerModel<T extends LivingEntity> extends SegmentedModel<T>
{
	public ModelRenderer topbun;
	public ModelRenderer hamburger;
	public ModelRenderer cheese;
	public ModelRenderer bottombun;

	public CheeseburgerModel()
	{
		this.textureWidth = 128;
		this.textureHeight = 64;
		this.topbun = new ModelRenderer(this, 60, 0);
		this.topbun.setRotationPoint(-9.0F, 12.0F, 7.5F);
		this.topbun.addBox(0.1F, 0.0F, -16.0F, 17, 5, 17, 0.0F);
		this.hamburger = new ModelRenderer(this, 67, 30);
		this.hamburger.setRotationPoint(-7.0F, 18.0F, 7.0F);
		this.hamburger.addBox(0.0F, 0.0F, -14.0F, 14, 2, 14, 0.0F);
		this.bottombun = new ModelRenderer(this, 0, 11);
		this.bottombun.setRotationPoint(-9.0F, 20.0F, 7.5F);
		this.bottombun.addBox(0.1F, 0.0F, -16.0F, 17, 4, 17, 0.0F);
		this.cheese = new ModelRenderer(this, 0, 41);
		this.cheese.setRotationPoint(-8.0F, 17.0F, 7.0F);
		this.cheese.addBox(0.0F, 0.0F, -15.0F, 16, 1, 16, 0.0F);
	}

	@Override
	public Iterable<ModelRenderer> getParts()
	{
		return ImmutableList.of(this.topbun, this.hamburger, this.bottombun, this.cheese);
	}

	public void setRotateAngle(ModelRenderer modelRenderer, float x, float y, float z)
	{
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}

	@Override
	public void setRotationAngles(T entityIn, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch)
	{
		this.topbun.rotateAngleX = (MathHelper.cos(ageInTicks * 0.2F) * 0.1F) - 0.1F;
	}

}
