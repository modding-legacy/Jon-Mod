package me.xjon.jons_mod.client;

import me.xjon.jons_mod.client.render.CheeseburgerRenderer;
import me.xjon.jons_mod.registry.JonEntityTypes;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraftforge.fml.client.registry.IRenderFactory;
import net.minecraftforge.fml.client.registry.RenderingRegistry;

/**
 * Used to register new entity renders. Every entity needs one of these,
 * otherwise the entity will not render.
 */
public class JonEntityRendering
{
	public static void init()
	{
		register(JonEntityTypes.CHEESEBURGER, CheeseburgerRenderer::new);
	}

	private static <T extends Entity> void register(EntityType<T> entityClass, IRenderFactory<? super T> renderFactory)
	{
		RenderingRegistry.registerEntityRenderingHandler(entityClass, renderFactory);
	}
}